<?php

?>
<!doctype html>
<html lang="cs">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--<link rel="profile" href="https://gmpg.org/xfn/11" /> -->
    <link rel="stylesheet" href="https://use.typekit.net/bri7qhp.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/t/css/style.css">
</head>
<body>
<header>
    <div class="container">
        <nav>
            <div class="leftSide">
                <div class="logo">
                    <img src="img/logo_keya.png">
                </div>
            </div>
            <div class="rightSide">
                <div class="links">
                    <li><a href="#about">kdo jsme</a></li>
                    <li><a href="">historie</a></li>
                    <li><a href="">foto</a></li>
                    <li><a href="">kontakt</a></li>
                </div>
            </div>
        </nav>
    </div>
</header>